(* ::Package:: *)

(* Abort for old, unsupported versions of Mathematica *)
If[$VersionNumber < 10,
  Print["sylvesterMatrix requires Mathematica 10.0 or later."];
  Abort[]
];

BeginPackage["sylvesterMatrix`", {"manyBosonStates`"}];

Unprotect @@ Names["sylvesterMatrix`*"];
ClearAll @@ Names["sylvesterMatrix`*"];

sylvesterMatrix::usage = "sylvesterMatrix[n] returns the nxn (exact) Sylvester matrix.";
sylvesterMatrixN::usage =
    "sylvesterMatrixN[n] returns the n dimensional Sylvester matrix with machine precision elements.";
sylvesterMatrixNormalized::usage = "sylvesterMatrixNormalized[n] returns the n dimensional, machine precision, Sylvester matrix normalized so to have all elements equal to either 1 or -1.";
sylvesterPermContribution::usage = "sylvesterPermContribution[inputState,outputState,permutation] \
returns the contribution of the permutation to the permanent determining the scattering amplitude of \
inputState->outputState.";
sylvesterBinaryMatrixProduct::usage = "sylvesterBinaryMatrixProduct[inputState,outputState] returns a \
display of the various contributions to the scattering amplitude of going from inputState to \
outputState.";
binaryMatrixNegateColumns::usage = "binaryMatrixNegateColumns[inputState,columns] returns a state differing from inputState for the negation of the specified columns in the binaryMatrix form.";
binaryMatrixNeutralNegations::usage = "binaryMatrixNeutralNegations[inputState] returns the sets of columns which can be negated in the BM form of inputState without changing the represented state.";
binaryMatrixPermuteColumns::usage = "binaryMatrixPermuteColumns[inputState,permutation] returns the state obtained by applying the specified permutation to the columns of inputState.
binaryMatrixPermuteColumns[inputState] returns the set of states differing from inputState for a different permutation of the columns in the binary matrix form.";
binaryMatrixPermuteRows::usage = "binaryMatrixPermuteRows[inputState, permutation] returns the state obtained by permuting the rows of the corresponding binary matrix according to the specified permutation.";
selectEvenColumns::usage = "selectEvenColumns[matrix] returns the set of columns having an even number of 1s.";
highlightEvenColumns::usage = "highlightEvenColumns[matrix] highlight all the columns of the matrix with an even number of 1s.";
evenBinaryMatrixQ::usage = "evenBinaryMatrixQ[state] returns True if the state has in Binary Matrix form an equal number of 1s and 0s in each column.";
listEvenCFMOLs::usage = "listEvenCFMOLs[m,n] returns the states, in MOL form, having an even number of 1s in each column of their BM form.";
listOddCFMOLs::usage = "listOddCFMOLs[m,n] returns the states, in MOL form, having an even number of 1s in each column of their BM form.";
negateBit::usage = "negateBit[n] returns the 1 if n==0 and 0 otherwise.";
sylvesterSuppressedQ::usage = "sylvesterSuppressedQ[inputState,outputState] returns True if the output state is suppressed according to the suppression law when entering with inputState.";
sssBruteForceSuppressionRates::usage = "sssBruteForceSuppressionRates[m,n,nSamples] returns the number of suppressed input-output pairs of n photons into m modes many-boson states, found uniformly sampling among input and output collision-free states.";
sssPredictedSuppressionRates::usage = "sssPredictedSuppressionRates[m,n,nSamples] returns the number of suppressed pairs among with the number of suppressed pairs predicted with the efficient suppression-law.";
sssPredictedSuppressionRatesPlot::usage = "sssPredictedSuppressionRatesPlot[m,nMax,nSamples] gives a plot of predicted suppressed input-output pairs agains the total number found.";
sssSuppressionLawPredictionRate::usage = "sssSuppressionLawPredictionRate[m,n,nSamples] returns the fraction of suppressed pairs which would have been predicted by the suppression law.";


Begin["`Private`"];
Needs["manyBosonStates`"];


ClearAll[hMatrix, sylvesterMatrix];
hMatrix[1] = 1;
hMatrix[2] = {{1, 1}, {1, -1}};
hMatrix[m_Integer?(IntegerQ[Log[2, #]]&)] :=
    hMatrix[m] = Join[
      Join[hMatrix[m / 2], hMatrix[m / 2], 2],
      Join[hMatrix[m / 2], -hMatrix[m / 2], 2],
      1
    ];

sylvesterMatrix[m_Integer?(IntegerQ[Log[2, #]]&)] := sylvesterMatrix[m] = hMatrix[m] / Sqrt@m;
sylvesterMatrixN[m_Integer?(IntegerQ[Log[2, #]]&)] := sylvesterMatrixN[m] = hMatrix[m] / Sqrt@m // N;
sylvesterMatrixNormalized[m_Integer?(IntegerQ[Log[2, #]]&)] := hMatrix[m] // N;


Options[sylvesterPermContribution] = {whatToPermute -> "input"};
SyntaxInformation[sylvesterPermContribution] = {"ArgumentsPattern" -> {_, _, _, OptionsPattern[]}};
sylvesterPermContribution[inputState_?manyBodyStateQ, outputState_?manyBodyStateQ, permutation_List?PermutationListQ, opts : OptionsPattern[]] := With[{input = First@toBM@inputState, output = First@toBM@outputState},
  Which[
    OptionValue@whatToPermute == "output",
    (-1)^Total[Total[input * output[[permutation, All]]]],
    OptionValue@whatToPermute == "input",
    (-1)^Total[Total[input[[permutation, All]] * output]]
  ]
]
sylvesterPermContribution[inputState_, outputState_, permutations : {__?PermutationListQ}, opts : OptionsPattern[]] := Table[sylvesterPermContribution[inputState, outputState, perm, opts], {perm, permutations}]


Options[sylvesterBinaryMatrixProduct] = {whatToPermute -> "output"};
SyntaxInformation[sylvesterBinaryMatrixProduct] = {"ArgumentsPattern" -> {_, _, OptionsPattern[]}};
sylvesterBinaryMatrixProduct[inputState_?manyBodyStateQ, outputState_?manyBodyStateQ, opts : OptionsPattern[]] := With[{input = First@toBM@inputState, output = First@toBM@outputState},
  Column[{
    Row[{
      Style["Input state:", Red, Bold],
      MatrixForm@input,
      Spacer@100,
      Style["Output state:", Red, Bold],
      MatrixForm@output
    }],
    Multicolumn[
      Table[
        Module[{permutedMatrix, otherMatrix},
          Which[
            OptionValue@whatToPermute === "output",
            permutedMatrix = output[[perm, All]];
            otherMatrix = input,
            OptionValue@whatToPermute === "input",
            permutedMatrix = input[[perm, All]];
            otherMatrix = output
          ];
          Column[{
            perm,
            ReplacePart[permutedMatrix, {i_, j_} /; otherMatrix[[i, j]]permutedMatrix[[i, j]] == 1 :> Item[permutedMatrix[[i, j]], Background -> Orange]] // MatrixForm,
            If[EvenQ[#], Style[#, Darker@Green, Bold], Style[#, Red, Bold]]&@Total@Total[permutedMatrix * otherMatrix]
          }, Center, 0.5
          ]
        ],
        {perm, Permutations@Range@Length@input}
      ],
      12,
      Frame -> All,
      Appearance -> "Horizontal"
    ]
  }, Left, 2]
]


Attributes[negateBit] = {Listable};
negateBit[b_Integer] := If[b == 0, 1, 0]


SyntaxInformation[binaryMatrixNegateColumns] = {"ArgumentsPattern" -> {_, _}};

binaryMatrixNegateColumns[state_?manyBodyStateQ, col_Integer] := With[
  {matrix = First @ toBM @ state},
  toSameForm[state, #]& @ ReplacePart[
    matrix,
    {i_, col} :> (matrix[[i, col]] /. {1 -> 0, 0 -> 1})
  ]
];

binaryMatrixNegateColumns[matrix : {{__Integer}..}, cols : {___Integer}] := Table[If[MemberQ[cols, k], negateBit@#[[k]], #[[k]]], {k, Length@#}]&@Transpose@matrix // Transpose

binaryMatrixNegateColumns[state_?manyBodyStateQ, cols : {___Integer}] := With[
  {matrix = First @ toBM @ state},
  toSameForm[state, #]& @ Transpose @ Table[
    If[MemberQ[cols, k],
      negateBit @ #[[k]], #[[k]]
    ],
    {k, Length @ #}
  ]& @ Transpose @ matrix
];


SyntaxInformation[binaryMatrixNeutralNegations] = {"ArgumentsPattern" -> {_}};

binaryMatrixNeutralNegations[stateMOL : {__Integer}] := With[{
    matrix = First @ toBM @ stateMOL
  },
  Rest @ Select[
    Subsets @ Range @ Log[2, Length @ stateMOL],
    And[
      Total @ Total @ matrix[[All, #]] == Length @ # * Length @ matrix / 2,
      SortBy[binaryMatrixNegateColumns[matrix, #], FromDigits[#, 2]&] == matrix
    ]&
  ]
];

binaryMatrixNeutralNegations[state_?manyBodyStateQ] := With[
  {matrix = First @ toBM @ state},
  Rest @ Select[
    Subsets @ Range @ Length @ Transpose @ matrix,
    toMOL @ matrix == toMOL @ binaryMatrixNegateColumns[matrix, #]&
  ]
];


binaryMatrixPermuteColumns[
  state_ ? manyBodyStateQ, sigma_List ? PermutationListQ
] := toSameForm[state, First[toBM @ state][[All, sigma]]];

(* If a single argument is given, all the (different) matrices obtained by
   permuting some subset of columns are returned *)
binaryMatrixPermuteColumns[state_ ? manyBodyStateQ] := Union @ Table[
  binaryMatrixPermuteColumns[state, sigma],
  {sigma, Permutations @ Range @ Log[2, nOfModes @ state]}
];


binaryMatrixPermuteRows[
  state_ ? manyBodyStateQ,
  sigma_List ? PermutationListQ
] := toSameForm[state, First[toBM @ state][[sigma]]];


selectEvenColumns[matrix : {{__Integer}..}] := Select[
  Range @ Length @ Transpose @ matrix,
  EvenQ @ Total[matrix[[All, #]]]&
];

selectEvenColumns[state_?manyBodyStateQ] := selectEvenColumns[
  First @ toBM @ state
];


highlightEvenColumns[matrix : {{__Integer}..}] := MatrixForm @ ReplacePart[
  matrix,
  Table[
    With[{j = j},
      {i_, j} :> Item[matrix[[i, j]], Background -> Orange]
    ],
    {j, selectEvenColumns @ matrix}
  ]
];

highlightEvenColumns[state_ ? manyBodyStateQ] := highlightEvenColumns[
  First @ toBM @ state
];


evenBinaryMatrixQ[matrix : {{__Integer}..}] := And @@ Thread[
  Total @ matrix == Length @ matrix / 2
];

evenBinaryMatrixQ[state_?manyBodyStateQ] := And @@ Thread[
  Total @ First @ toBM @ state == Length @ First @ toBM @ state / 2
];


listEvenCFMOLs[m_Integer, n_Integer] := Select[listCFMOLs[m, n],
  evenBinaryMatrixQ
];


listOddCFMOLs[m_Integer, n_Integer] := Select[listCFMOLs[m, n],
  Not @* evenBinaryMatrixQ
];


sylvesterSuppressedQ[inputMOL : {__Integer}, outputMOL : {__Integer}] := Module[{flag = False},
  If[OddQ@Total@inputMOL, False,
    If[
      Do[
        If[OddQ@Total@Flatten@First[toBM@outputMOL][[All, cols]], flag = True; Return[], False],
        {cols, binaryMatrixNeutralNegations@inputMOL}
      ];
      flag, True,
      flag = False;Do[
        If[OddQ@Total@Flatten@First[toBM@inputMOL][[All, cols]], flag = True;Return[], False],
        {cols, binaryMatrixNeutralNegations@outputMOL}
      ];flag
    ]
  ]
];

sylvesterSuppressedQ[inputState_?manyBodyStateQ, outputState_?manyBodyStateQ] := If[OddQ@nOfPhotons@inputState, False,
  SelectFirst[
    binaryMatrixNeutralNegations[inputState],
    OddQ@Total@Flatten@First[toBM@outputState][[All, #]]&,
    False
  ] // If[# =!= False, True,
    SelectFirst[
      binaryMatrixNeutralNegations[outputState],
      OddQ@Total@Flatten@First[toBM@inputState][[All, #]]&,
      False
    ] // If[# =!= False, True, False]&
  ]&
];


Options[sssBruteForceSuppressionRates] = {outputFormat -> "Default"};
sssBruteForceSuppressionRates[m_Integer, n_Integer, nSamples_Integer : 1000, OptionsPattern[]] := Module[{
  sampledInput, sampledOutput,
  suppressedCount = 0, iterationIndex = 0},
  Do[
    sampledInput = randomCFMOL[m, n];
    sampledOutput = randomCFMOL[m, n];
    If[
      suppressedQ[sampledInput, sampledOutput, sylvesterMatrixNormalized],
      suppressedCount++
    ],
    {iterationIndex, nSamples}
  ] ~ Monitor ~ (Row[{
    ProgressIndicator[#, {0, 100}],
    Style["m=" <> ToString@m <> ", n=" <> ToString@n <> ",  progress=" <> ToString@# <> "%", Bold]
  }, " "
  ]&@(100 iterationIndex / nSamples // N))
  ;Which[
    ToLowerCase@OptionValue@outputFormat == "ratio",
    N[suppressedCount / nSamples],
    # == "percentage" || # == "%"&@ToLowerCase@OptionValue@outputFormat,
    N[100suppressedCount / nSamples],
    True,
    suppressedCount
  ]
];

Options[sssPredictedSuppressionRates] = {outputFormat -> "Default"};
sssPredictedSuppressionRates[m_Integer, n_Integer, nSamples_Integer : 1000, OptionsPattern[]] := Module[{
  sampledInput, sampledOutput,
  suppressedCount = 0, predictedSuppressedCount = 0,
  iterationIndex = 0},
  Monitor[
    Do[
      sampledInput = randomCFMOL[m, n];
      sampledOutput = randomCFMOL[m, n];
      If[
        suppressedQ[sampledInput, sampledOutput, sylvesterMatrixNormalized],
        suppressedCount++;
        If[
          sylvesterSuppressedQ[sampledInput, sampledOutput],
          predictedSuppressedCount++
        ]
      ],
      {iterationIndex, nSamples}
    ],
    Row[{
      ProgressIndicator[#, {0, 100}],
      Style["m=" <> ToString@m <> ", n=" <> ToString@n <> ",  progress=" <> ToString@# <> "%", Bold]
    }, " "
    ]&@(100 iterationIndex / nSamples // N)
  ];
  Which[
    ToLowerCase@OptionValue@outputFormat == "ratio",
    {n, N[suppressedCount / nSamples], N[ predictedSuppressedCount / nSamples]},
    # == "percentage" || # == "%"&@ToLowerCase@OptionValue@outputFormat,
    {n, N[100suppressedCount / nSamples], N[100 predictedSuppressedCount / nSamples]},
    True,
    {n, suppressedCount, predictedSuppressedCount}
  ]
];

Options[sssPredictedSuppressionRatesPlot] = {outputFormat -> "default", whichPhotonNumbers -> "Even"};
sssPredictedSuppressionRatesPlot[m_Integer, nMax_Integer, nSamples_Integer : 1000, opts : OptionsPattern[]] := With[{data = Table[
  sssPredictedSuppressionRates[m, n, nSamples, FilterRules[{opts}, Options[sssPredictedSuppressionRates]]],
  {n, Which[
    ToLowerCase@OptionValue@whichPhotonNumbers == "even", Range[2, nMax, 2],
    ToLowerCase@OptionValue@whichPhotonNumbers == "all", Range[1, nMax]
  ]}
]},
  Graphics[{
    PointSize@0.01,
  (* REAL DATA *)
    Red, Point@data[[All, {1, 2}]], Line@data[[All, {1, 2}]],
    Opacity@0.1, Polygon@Join[{{data[[1, 1]], 0}}, data[[All, {1, 2}]], {{data[[Length@data, 1]], 0}, {data[[1, 1]], 0}}],
  (* EFFICIENTLY PREDICTED DATA *)
    Opacity@1, Green, Point@data[[All, {1, 3}]], Line@data[[All, {1, 3}]],
    Opacity@0.1, Polygon@Join[data[[All, {1, 3}]], {{data[[Length@data, 1]], 0}, {data[[1, 1]], 0}}]
  },
    PlotRange -> All,
    PlotRangePadding -> 0.1,
    ImageSize -> Large,
    AspectRatio -> 1,
    Axes -> True,
    AxesOrigin -> {If[ToLowerCase@OptionValue@whichPhotonNumbers == "even", 1.8, 1], 0},
    AxesLabel -> {n, "suppression %"},
    Ticks -> {Range[2, nMax, 2], Automatic},
    LabelStyle -> Directive[Bold, Medium]
  ]
];


Options[sssSuppressionLawPredictionRate] = {outputFormat -> "Default"};

sssSuppressionLawPredictionRate[
  m_Integer, n_Integer, nSamples_Integer : 1000, OptionsPattern[]
] := Module[{
  sampledInput, sampledOutput,
  suppressedCount = 0, predictedSuppressedCount = 0,
  iterationIndex = 0},
  Monitor[
    Do[
      sampledInput = randomCFMOL[m, n];
      sampledOutput = randomCFMOL[m, n];
      If[
        suppressedQ[sampledInput, sampledOutput, sylvesterMatrixNormalized],
        suppressedCount++;
        If[
          sylvesterSuppressedQ[sampledInput, sampledOutput],
          predictedSuppressedCount++
        ]
      ],
      {iterationIndex, nSamples}
    ],
    Row[{
      ProgressIndicator[#, {0, 100}],
      Style["m=" <> ToString@m <> ", n=" <> ToString@n <> ",  progress=" <> ToString@# <> "%", Bold]
    }, " "
    ]&@(100 iterationIndex / nSamples // N)
  ];
  Which[
    # == "%" || # == "percentage"&@ToLowerCase@OptionValue@outputFormat,
    N[100predictedSuppressedCount / suppressedCount],
    True,
    N[predictedSuppressedCount / suppressedCount]
  ]
];


(* Protect all package symbols *)
With[{syms = Names["sylvesterMatrix`*"]}, SetAttributes[syms, {Protected, ReadProtected}] ];
Unprotect[sylvesterMatrix, sylvesterMatrixN];

End[];
EndPackage[];
